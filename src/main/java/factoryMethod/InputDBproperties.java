package factoryMethod;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author Joy
 */
public class InputDBproperties {
    
    private String  brand;
    private String  host;
    private String  dbTable;
    private String  dbName;
    private String  post;
    private String  username;
    private String  password;

    public InputDBproperties() {
    }

    
    public InputDBproperties(String brand, String host, String dbTable, String dbName, String post, String username, String password) {
        this.brand = brand;
        this.host = host;
        this.dbTable = dbTable;
        this.dbName = dbName;
        this.post = post;
        this.username = username;
        this.password = password;
    }
   
    public void setDB(){
    
        Properties prop = new Properties();
	InputStream input = null;

	try {

		input = new FileInputStream("src/main/resources/DB.properties");

		// load a properties file
		prop.load(input);

		// get the property value and print it out
                setBrand(prop.getProperty("BRAND"));
                setDbName(prop.getProperty("DB_Name"));
                setDbTable(prop.getProperty("DB_TABLE"));
                setHost(prop.getProperty("HOST"));
                setPassword(prop.getProperty("Password"));
                setUsername(prop.getProperty("User_Name"));
                setPost(prop.getProperty("PORT_Number"));

	} catch (IOException ex) {
		ex.printStackTrace();
	} finally {
		if (input != null) {
			try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

    
    
    }

    public String getBrand() {
        return brand;
    }

    public String getHost() {
        return host;
    }

    public String getDbTable() {
        return dbTable;
    }

    public String getDbName() {
        return dbName;
    }

    public String getPost() {
        return post;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setDbTable(String dbTable) {
        this.dbTable = dbTable;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
