package factoryMethod;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.Connection;
import java.sql.ResultSet;

/**
 *
 * @author Joy
 */
public interface DBConnection {
    
    ResultSet executeQueryDB(String query);
    Boolean updateDB(String command);
    void closeDB();
    Connection getDBConnection();
}
