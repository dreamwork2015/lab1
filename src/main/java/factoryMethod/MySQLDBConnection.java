package factoryMethod;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Joy
 */
public class MySQLDBConnection implements DBConnection {
    
    private Connection connection;
    private String url,user,password;
    public MySQLDBConnection(String host,String post,String dbName ,String user
    ,String password) {
       
        try 
        {
            this.url = url;
            this.user = user;
            this.password = password;
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://" + host + ":" + post + "/" + dbName ;
            connection = DriverManager.getConnection(url,user,password);
            
        } catch (SQLException | ClassNotFoundException ex) {
            
           Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        
        
    }

    @Override
    public ResultSet executeQueryDB(String query) {
        try 
        {
            Statement st = connection.createStatement();
            ResultSet result = st.executeQuery(query);
            return result;
        } catch (SQLException ex) {
           Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public Boolean updateDB(String command) {
      
        try 
        {
            Statement st = connection.createStatement();
            st.executeUpdate(command);
            return true;
        } catch (SQLException ex) {
          Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
           
      }

    @Override
    public void closeDB() {
        try 
        {
            if(connection !=null){
                 connection.close();
            }
           
        } catch (SQLException ex) {
            Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public Connection getDBConnection() {


		try {
                         Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}

		try {
                        connection = DriverManager.getConnection(url,user,password);
			return connection;

		} catch (SQLException e) {

			System.out.println(e.getMessage());
		}

		return connection;

	}


}
