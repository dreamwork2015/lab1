package factoryMethod;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */




import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Joy
 */
public class TestDBConnection {

    public static void main(String[] args) throws SQLException{

        DBConnectionFactory  dbFactory = new DBConnectionFactory();
        DBConnection dbConn= dbFactory.createDBContion("mysql","www.db4free.net","3306", "cs386cstu","cs386user", "rongviri");
        
        if(dbConn !=null){
              ResultSet results = dbConn.executeQueryDB("select * from LoginAccount");
              while (results.next())
               {    
                       System.out.println("User Name: " + results.getString(1));
                       System.out.println("Password:  " + results.getString(2));
               }
		
             }
   
	dbConn.closeDB();

        
    }

    
            
}
