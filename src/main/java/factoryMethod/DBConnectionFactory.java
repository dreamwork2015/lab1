package factoryMethod;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import factoryMethod.MySQLDBConnection;
import factoryMethod.DBConnection;


/**
 *
 * @author Joy
 */
public class DBConnectionFactory {
    
    public DBConnection createDBContion(String brand,String host,String port,
            String dbName,String username,String password){
        
        if(brand.equalsIgnoreCase("mysql")){
            return new MySQLDBConnection(host, port, dbName, username, password);
        }else{
            return null;
        }
    
    }
    
}
