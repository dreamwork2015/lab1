package com.mycompany.flightreservation.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.mycompany.flightreservation.domain.AirPort;
import com.mycompany.flightreservation.domain.AirPort;
import com.mycompany.flightreservation.domain.TravelClass;
import com.mycompany.flightreservation.domain.TravelClass;
import com.mycompany.flightreservation.domain.TripTypes;
import com.mycompany.flightreservation.domain.TripTypes;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.GregorianCalendar;


/**
 *
 * @author Joy&ggertgtrtrgrf 
 */
public class Flight {
    private String to;
    private String from;
    private String departDate;
    private String returnDate;
    private String destination;
    private byte travellers;
    private String travelClass;
    private String ID;

    public Flight(String id, String from,String to, String departDate, String returnDate, byte  travellers, String classs,String destination) {
        ID = id;
        this.to = to;
        this.from = from;
        this.departDate = departDate;
        this.returnDate = returnDate;
        this.destination = destination;
        this.travellers = travellers;
        this.travelClass = classs;
    }

    public String getTo() {
        return to;
    }

    public String getFrom() {
        return from;
    }

    public String getDepartDate() {
        return departDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public String getDestination() {
        return destination;
    }

    public byte  getTravellers() {
        return travellers;
    }

    public String getTravelClass() {
        return travelClass;
    }

    public String getID() {
        return ID;
    }
    
  
}
