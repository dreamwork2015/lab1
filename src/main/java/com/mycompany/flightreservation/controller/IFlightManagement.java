package com.mycompany.flightreservation.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.flightreservation.domain.Trip;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Joy&June
 */
public interface IFlightManagement {
    public List<Flight> searchFlight(Trip trip)  throws SQLException;
}
