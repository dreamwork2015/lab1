package com.mycompany.flightreservation.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Joy&June
 */
public class MissingRequiredTripInfoException extends Exception{

    public MissingRequiredTripInfoException(String message) {
        super(message);
    }
    
}
