package com.mycompany.flightreservation.view;

import com.mycompany.flightreservation.domain.ListModelJList.ListDetailsPayment;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.border.EtchedBorder;

public class MyPaymentListCellRenderer extends JPanel implements ListCellRenderer<ListDetailsPayment> {

	Double totalAdditionalServices=0.00;

	public MyPaymentListCellRenderer() {
		//this.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
	}

	@Override
	public Component getListCellRendererComponent(JList list,ListDetailsPayment value,
			int index, boolean isSelected, boolean cellHasFocus) {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.removeAll();

		JLabel price = new JLabel("Price:  " + value.getPrice() +" ( "+ value.getSeat() +" Seat)");
		JLabel taxes = new JLabel("Taxes:  " + value.getTaxes());
		JLabel serviceCharge = new JLabel("Service charge:   "+ value.getServiceCharge());
		JLabel reduction = new JLabel("Reduction:   " + value.getReduction());
		totalAdditionalServices = Double.valueOf(value.getSMS())+Double.valueOf(value.getInsurance()+Double.valueOf(value.getBackMoney()));
		JLabel additionalServices = new JLabel("Additional services:  " + totalAdditionalServices);
		JLabel sms = new JLabel("SMS: " + value.getSMS());
		JLabel service = new JLabel("Service ");
		JLabel supplementation = new JLabel("Supplementation:  " +String.valueOf(Double.valueOf(value.getInsurance()+Double.valueOf(value.getBackMoney()))));
                JLabel insurance = new JLabel("      -Insurance :  "+ value.getInsurance());
                JLabel moneyback = new JLabel("      -Moneyback :  "+ value.getBackMoney());
		JLabel total = new JLabel("Total amount:	 " + value.getTotal());
                
                
                // set Font
                price.setFont(new Font(null,0,14));
                taxes.setFont(new Font(null,0,14));
                serviceCharge.setFont(new Font(null,0,14));
                reduction.setFont(new Font(null,0,14));
                additionalServices.setFont(new Font(null,0,14));
                sms.setFont(new Font(null,0,14));
                service.setFont(new Font(null,0,14));
                supplementation.setFont(new Font(null,0,14));
                insurance.setFont(new Font(null,0,14));
                moneyback.setFont(new Font(null,0,14));
		total.setFont(new Font(null,Font.BOLD,15));
                total.setForeground(Color.BLUE);
		this.add(price);
		this.add(taxes);
		this.add(serviceCharge);
		this.add(reduction);
		if(value.getAdditionalServices()>0.00){
			this.add(additionalServices);
                        if(value.getSMS()>0.00){
                            this.add(sms);
                        }
			
			if(value.getInsurance()>0.00||value.getBackMoney()>0.00){
                         this.add(service);
                         this.add(supplementation);
                        }
                        if(value.getInsurance()>0.00){
                           
                            this.add(insurance);
                        }
                        if(value.getBackMoney()>0.00){
                             this.add(moneyback);
                        }
                     
                        
		}
		this.add(total);
            
                this.setBackground(Color.WHITE);
		return this;

	}

}
