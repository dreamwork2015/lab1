package com.mycompany.flightreservation.view;

import com.mycompany.flightreservation.domain.ListModelView.FlightDetail;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.EtchedBorder;

/**
 *
 * @author Joy
 */
public class MyFlightListCellRenderer extends JPanel
        implements ListCellRenderer<FlightDetail> {
    
    
    private ImageIcon icon ;
    private int count=0;
    private JButton b;
    
    public MyFlightListCellRenderer() {
        this.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));  
       
    }
    

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
    }
    
    @Override
    public Component getListCellRendererComponent(JList list, FlightDetail value,
            int index, boolean isSelected, boolean cellHasFocus) {
      
        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        this.removeAll();
        this.setPreferredSize(new Dimension(789,40));
        double price = value.getInboundFlightPrice()+value.getOutboundFlightPrice();
        JLabel priceLabel = new JLabel(String.valueOf(price));
        priceLabel.setFont(new Font("Courier New", Font.BOLD, 20));
        priceLabel.setForeground(Color.BLUE);
        priceLabel.setPreferredSize(new Dimension(90, 20));
        priceLabel.setMinimumSize(new Dimension(100, 20));
        priceLabel.setMaximumSize(new Dimension(100, 20));
        
        this.add(priceLabel);

        Component rigidArea1= Box.createRigidArea(new Dimension(20, 20));
        this.add(rigidArea1);
        this.setBorder(BorderFactory.createLineBorder(Color.black));
        
        String airlineLogo = value.getAirlinePic();
        String airlineName = value.getInboundAirCompany();
        String takeoffAirlineName = value.getInboundTakeOffAirport();
        String takeoffDateTime = value.getInboundTakeOffDateTime();
        String landingAirlineName = value.getInboundLandingAirport();
        String landingAirlineDateTime = value.getInboundLandingDateTime();

        
        icon = new ImageIcon("src\\main\\resources\\" + airlineLogo+".png");
        JLabel airlineLogoLabel = new JLabel(icon, SwingConstants.LEADING);
        JLabel airlineNameLabel = new JLabel(airlineName+"     ");
        JLabel takeoffAirlineNameLabel = new JLabel(takeoffAirlineName+"             ");
        takeoffAirlineNameLabel.setPreferredSize(new Dimension(200, 20));

        JLabel takeoffDateTimeLabel = new JLabel(takeoffDateTime+"                     ");
        JLabel landingAirlineDateTimeLabel = new JLabel(landingAirlineDateTime+"     ");
        b = new JButton("เลือก");
        
        this.add(airlineLogoLabel);
        this.add(airlineNameLabel);
        this.add(takeoffAirlineNameLabel);
        this.add(takeoffDateTimeLabel);
        this.add(landingAirlineDateTimeLabel);
        this.add(b);
      
        return this;
   
    }


 
    
}

