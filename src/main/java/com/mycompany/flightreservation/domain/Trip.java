package com.mycompany.flightreservation.domain;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 *
 * @author Joy&June
 */
public class Trip {
    
    private AirPort fromAirPort ; 
    private AirPort toAirport ;
    private String departureDateTime ;
    private String  returnDateTime ;
    private byte noOfPassengers ; 
    private TravelClass travelClass ;
    private TripTypes tripType ;
    private SimpleDateFormat simple = new SimpleDateFormat("YYYY-MM-dd",Locale.ENGLISH);
    
    
    public Trip(AirPort fromAirPort,AirPort toAirport,byte noOfPassengers,TravelClass travelClass, TripTypes tripType,Date  departureDateTime,Date returnDateTime) {

        this.fromAirPort = fromAirPort;
        this.toAirport = toAirport;
        this.noOfPassengers = noOfPassengers;
        this.travelClass = travelClass;
        this.tripType = tripType;
        this.departureDateTime = simple.format(departureDateTime);
        this.returnDateTime = simple.format(returnDateTime);
       

    }

    
    public AirPort getFromAirPort() {
        return fromAirPort;
    }

    public AirPort getToAirport() {
        return toAirport;
    }

    public byte getNoOfPassengers() {
        return noOfPassengers;
    }

    public  TravelClass getTravelClass() {
        return travelClass;
    }

    public TripTypes getTripType() {
        return tripType;
    }

    public String  getDepartureDateTime() {
        return departureDateTime;
    }

    public String  getReturnDateTime() {
        return returnDateTime;
    }


 
}
