package com.mycompany.flightreservation.domain.ListModelView;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;
import javax.swing.AbstractListModel;

/**
 *
 * @author Joy
 */
public class MyFlightListModel extends AbstractListModel<FlightDetail>{

    private ArrayList<FlightDetail> model;

    public MyFlightListModel() {
            model = new ArrayList<FlightDetail>();
    }

    public MyFlightListModel(ArrayList<FlightDetail> model) {
        this.model = model;
    }
    
    @Override
    public int getSize() {
        return model.size();
    }

    @Override
    public FlightDetail getElementAt(int index) {
        return model.get(index);
    }
    
    public void add(FlightDetail list){
        if(list!=null){   
            model.add(list);
        }
    }
    
    
}
