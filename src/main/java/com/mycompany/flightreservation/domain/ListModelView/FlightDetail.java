package com.mycompany.flightreservation.domain.ListModelView;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import java.lang.annotation.Annotation;
import javax.swing.JButton;
import org.jdesktop.swingx.JXButton;

/**
 *
 * @author Joy
 */
public class FlightDetail {
    
    
    
    private double inboundFlightPrice;
    private double outboundFlightPrice;
    private String inboundAirCompany;
    private String outboundAirCompany;
    private String inboundTakeOffAirport;
    private String outboundTakeOffAirport;
    private String inboundLandingAirport;
    private String outboundLandingAirport;
    private String inboundTakeOffDateTime;
    private String outboundTakeOffDateTime;       
    private String inboundLandingDateTime;       
    private String outboundLandingDateTime;
    private String airlinePic;
    private int id;

    public FlightDetail(int id ,double inboundFlightPrice, double outboundFlightPrice, String inboundAirCompany, String outboundAirCompany, String inboundTakeOffAirport, String outboundTakeOffAirport, String inboundLandingAirport, String outboundLandingAirport, String inboundTakeOffDateTime, String outboundTakeOffDateTime, String inboundLandingDateTime, String outboundLandingDateTime, String airlinePic) {
        this.inboundFlightPrice = inboundFlightPrice;
        this.outboundFlightPrice = outboundFlightPrice;
        this.inboundAirCompany = inboundAirCompany;
        this.outboundAirCompany = outboundAirCompany;
        this.inboundTakeOffAirport = inboundTakeOffAirport;
        this.outboundTakeOffAirport = outboundTakeOffAirport;
        this.inboundLandingAirport = inboundLandingAirport;
        this.outboundLandingAirport = outboundLandingAirport;
        this.inboundTakeOffDateTime = inboundTakeOffDateTime;
        this.outboundTakeOffDateTime = outboundTakeOffDateTime;
        this.inboundLandingDateTime = inboundLandingDateTime;
        this.outboundLandingDateTime = outboundLandingDateTime;
        this.airlinePic = airlinePic;
        this.id =id;
 
    }

    public int getId() {
        return id;
    }
    


    public double getInboundFlightPrice() {
        return inboundFlightPrice;
    }

    public double getOutboundFlightPrice() {
        return outboundFlightPrice;
    }

    public String getInboundAirCompany() {
        return inboundAirCompany;
    }

    public String getOutboundAirCompany() {
        return outboundAirCompany;
    }

    public String getInboundTakeOffAirport() {
        return inboundTakeOffAirport;
    }

    public String getOutboundTakeOffAirport() {
        return outboundTakeOffAirport;
    }

    public String getInboundLandingAirport() {
        return inboundLandingAirport;
    }

    public String getOutboundLandingAirport() {
        return outboundLandingAirport;
    }

    public String getInboundTakeOffDateTime() {
        return inboundTakeOffDateTime;
    }

    public String getOutboundTakeOffDateTime() {
        return outboundTakeOffDateTime;
    }

    public String getInboundLandingDateTime() {
        return inboundLandingDateTime;
    }

    public String getOutboundLandingDateTime() {
        return outboundLandingDateTime;
    }

    public String getAirlinePic() {
        return airlinePic;
    }


}
