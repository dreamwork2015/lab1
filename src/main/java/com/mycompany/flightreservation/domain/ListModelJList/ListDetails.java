/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.domain.ListModelJList;

import java.util.Observable;

public class ListDetails extends Observable {

	// Account balance
	private double price;
	private double tax;
	private double totalPrice,additionalServices;	
	private double serviceCharge;
        private double SMS;
        private double insurance;
        private double moneyBack;
        private double reduction;
        private int seat;


	public ListDetails(double price) {
		setPrice(price);
	}
        
        private void setSeat(int accountSeat) {
		seat = accountSeat;
		setChanged();
		notifyObservers();
	}


	public int getSeat() {
		return seat;
	}

	
	public void seat(int amount) throws IllegalArgumentException {
		if (amount < 0) throw new IllegalArgumentException("Cannot withdraw negative amount");
		setSeat(amount);
	}

	private void setPrice(double accountPrice) {
		price = accountPrice;
		setChanged();
		notifyObservers();
	}


	public double getPrice() {
		return price* getSeat();
	}

	
	public void flightPrice(double amount) throws IllegalArgumentException {
		if (amount < 0) throw new IllegalArgumentException("Cannot withdraw negative amount");
		setPrice(amount);
	}
	
	
	
	private void setTax(double accountTax) {
		tax = accountTax;
		setChanged();
		notifyObservers();
	}
	
	public double getTax() {
		return tax;
	}


	public void taxes(double amount) throws IllegalArgumentException {
		if (amount < 0) throw new IllegalArgumentException("Cannot deposit negative amount");
		setTax((amount*getSeat())*7/100);
	}
	
	private void setServiceCharge(double accountServiceCharge) {
		serviceCharge = accountServiceCharge;
		setChanged();
		notifyObservers();
	}
	
	public double getServiceCharge() {
		return serviceCharge;
	}


	public void serviceCharge(double amount) throws IllegalArgumentException {
		if (amount < 0) throw new IllegalArgumentException("Cannot deposit negative amount");
		setServiceCharge((amount*getSeat())*10/100);
	}
	
	

	
	public double getAdditionalServices() {
		return getReduction()+getSMS();
	}

	
	private void setAdditionalServices(double accountAdditionalServices) {
		additionalServices = accountAdditionalServices;
		setChanged();
		notifyObservers();
	}

	

	public void SMS(){
		setSMS(25);
	}


	public double getSMS() {
		return SMS;
	}

	
	public  void setSMS(double sms) {
		SMS = sms;
		setChanged();
		notifyObservers();
	}

        public void reduction(double amount) throws IllegalArgumentException {
            if (amount < 0) throw new IllegalArgumentException("Cannot deposit negative amount");
		setReduction(amount);
	}


	public double getReduction() {
            return reduction;
	}

	
	public  void setReduction(double reduction) {
		this.reduction = reduction;
		setChanged();
		notifyObservers();
	}
        
        

	public void insurance(double amount) throws IllegalArgumentException {
		if (amount < 0) throw new IllegalArgumentException("Cannot deposit negative amount");
		setInsurance(amount);
	}
	public double getInsurance() {
		return insurance;
	}

	
	public void setInsurance(double insurance) {
		this.insurance = insurance;
		setChanged();
		notifyObservers();
	}

	public void moneyBack(double amount) throws IllegalArgumentException {
		if (amount < 0) throw new IllegalArgumentException("Cannot deposit negative amount");
		setMoneyBack(amount);
	}
	public double getMoneyBack() {
		return moneyBack;
	}

	
	public void setMoneyBack(double moneyBack) {
		this.moneyBack = moneyBack;
		setChanged();
		notifyObservers();
	}
	

	public void additionalServices(double amount) throws IllegalArgumentException {
		if (amount < 0)
			throw new IllegalArgumentException("Cannot deposit negative amount");
		setAdditionalServices(258);
	}
	
	public double getTotalPrice() {
		return totalPrice;
	}

	
	private void setTotalPrice(double accountTotalPrice) {
		totalPrice = accountTotalPrice;
		setChanged();
		notifyObservers();
	}

	

	public void totalPrice() throws IllegalArgumentException {
		setTotalPrice(getTax()+getPrice()+getServiceCharge()+getSMS()+getMoneyBack()+getInsurance()-getReduction());
	}

	
	
	
	
	
	
}
