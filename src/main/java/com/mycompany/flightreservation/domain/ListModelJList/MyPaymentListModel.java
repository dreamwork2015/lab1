package com.mycompany.flightreservation.domain.ListModelJList;

import java.util.ArrayList;
import javax.swing.AbstractListModel;

public class MyPaymentListModel extends AbstractListModel<ListDetailsPayment>{

	   private ArrayList<ListDetailsPayment> model;

	    public MyPaymentListModel() {
	        model = new ArrayList<ListDetailsPayment>();
	    }

	    public MyPaymentListModel(ArrayList<ListDetailsPayment> model) {
	        this.model = model;
	    }

	    @Override
	    public int getSize() {
	        return model.size();
	    }

	    @Override
	    public ListDetailsPayment getElementAt(int index) {
	        return model.get(index);
	    }
	    
	    public void add(ListDetailsPayment list){
	        if(list!=null){   
	            model.add(list);
	        }
	    }
	    
	    
	}