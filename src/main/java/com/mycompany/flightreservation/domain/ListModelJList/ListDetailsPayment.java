package com.mycompany.flightreservation.domain.ListModelJList;


import java.lang.annotation.Annotation;
import java.util.Observable;

public class ListDetailsPayment{

    private Double price;
    private Double taxes;
    private Double serviceCharge; 
    private Double reduction;     
    private Double SMS;
    private Double insurance;
    private Double total;
    private Double additionalServices;
    private Double backMoney;
    private int seat ;

    public ListDetailsPayment(Double price, Double taxes, Double serviceCharge, Double reduction, Double SMS, Double insurance, Double total, Double additionalServices, Double backMoney, int seat) {
        this.price = price;
        this.taxes = taxes;
        this.serviceCharge = serviceCharge;
        this.reduction = reduction;
        this.SMS = SMS;
        this.insurance = insurance;
        this.total = total;
        this.additionalServices = additionalServices;
        this.backMoney = backMoney;
        this.seat = seat;
    }

    public Double getPrice() {
        return price;
    }

    public Double getTaxes() {
        return taxes;
    }

    public Double getServiceCharge() {
        return serviceCharge;
    }

    public Double getReduction() {
        return reduction;
    }

    public Double getSMS() {
        return SMS;
    }

    public Double getInsurance() {
        return insurance;
    }

    public Double getTotal() {
        return total;
    }

    public Double getAdditionalServices() {
        return additionalServices;
    }

    public Double getBackMoney() {
        return backMoney;
    }

    public int getSeat() {
        return seat;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setTaxes(Double taxes) {
        this.taxes = taxes;
    }

    public void setServiceCharge(Double serviceCharge) {
        this.serviceCharge = serviceCharge;
    }

    public void setReduction(Double reduction) {
        this.reduction = reduction;
    }

    public void setSMS(Double SMS) {
        this.SMS = SMS;
    }

    public void setInsurance(Double insurance) {
        this.insurance = insurance;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public void setAdditionalServices(Double additionalServices) {
        this.additionalServices = additionalServices;
    }

    public void setBackMoney(Double backMoney) {
        this.backMoney = backMoney;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }
   
    
}