/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.domain.ListModelJList;

import java.awt.Color;
import java.util.*;
import javax.swing.JPanel;

public abstract class AbstractPaymentView extends JPanel implements Observer {

	
	private ListDetails  payment;

	
	public AbstractPaymentView(ListDetails  observablePayment)throws NullPointerException {

		
		if (observablePayment == null)
			throw new NullPointerException();
		
		payment = observablePayment;
		payment.addObserver(this);
                setBackground(Color.WHITE);
           
		//setBorder(new MatteBorder(1, 1, 1, 1, Color.black));
		
	}

	// get Account for which this view is an Observer
	public ListDetails getAccount() {
		return payment;
	}

	// update display with Account balance
	protected abstract void updateDisplay();

	// receive updates from Observable Account
	public void update(Observable observable, Object object) {
		updateDisplay();
	}
}
