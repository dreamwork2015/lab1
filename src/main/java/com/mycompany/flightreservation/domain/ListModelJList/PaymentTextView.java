/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.domain.ListModelJList;


import com.mycompany.flightreservation.view.MyPaymentListCellRenderer;
import java.awt.Color;
import java.util.*;
import java.awt.Dimension;
import java.text.NumberFormat;



// Java extension packages
import javax.swing.*;


public class PaymentTextView extends AbstractPaymentView {
	
	// JTextField for displaying Account balance


	private ListDetailsPayment data ;
	private ArrayList<ListDetailsPayment> model ;
	private MyPaymentListCellRenderer renderer;
        private JList<ListDetailsPayment> jList1;

	private NumberFormat moneyFormat = NumberFormat.getCurrencyInstance(Locale.US);


	public PaymentTextView(ListDetails payment,JList jList) {
		super(payment);
                jList1 = jList;
                model = new ArrayList<>();
                data = new ListDetailsPayment(0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0);
                model.add(data);
                jList1.setModel(new MyPaymentListModel(model)); 
                renderer = new MyPaymentListCellRenderer(); 
                jList1.setCellRenderer(renderer);
                setPreferredSize(new Dimension(150,600));
                updateDisplay();

	}


	public void updateDisplay() {

		data.setPrice(getAccount().getPrice());
                data.setSeat(getAccount().getSeat());
		data.setTaxes(getAccount().getTax());
		data.setServiceCharge(getAccount().getServiceCharge());
		data.setSMS(getAccount().getSMS());
		data.setInsurance(getAccount().getInsurance());
                data.setBackMoney(getAccount().getMoneyBack());
                data.setReduction(getAccount().getReduction());
                data.setAdditionalServices(getAccount().getSMS()+getAccount().getMoneyBack()+getAccount().getInsurance());
		data.setTotal(getAccount().getTotalPrice());
		model.clear();
		data = new ListDetailsPayment(data.getPrice(),data.getTaxes(),data.getServiceCharge(),data.getReduction(),data.getSMS(),data.getInsurance(),data.getTotal(),data.getAdditionalServices(),data.getBackMoney(),data.getSeat());
                model.add(data);
                renderer = new MyPaymentListCellRenderer(); 
                jList1.setCellRenderer(renderer);
            }


        
        
        
}

