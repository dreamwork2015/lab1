package com.mycompany.flightreservation.domain;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Joy&June
 */
public enum AirPort {
    BKK_Suvarnabhumi_Intl("Suvarnabhumi Intl.", "Bangkok (Bang Phli,Samut Prakan)" ,"TH"),
    DMK_DonMueang("Don Mueang International Airport", "Bangkok" ,"TH"),
    HKT_Phuket("Phuket International Airport", "Phuket" ,"TH"),
    CNX_ChiangMai("Chiang Mai International Airport", "Chiang Mai" ,"TH"),
    HDY_HatYai("Hat Yai International Airport", "Songkhla" ,"TH"),
    CEI_MaeFahLuangChiangRai("Mae Fah Luang Chiang Rai International Airport", "Chiang Rai" ,"TH"),
    LGW_Gatwick("Gatwick", "London (Crawley, West Sussex)" ,"UK"),
    MMA_All_Airports("All Airports", "Sweden (Malmö, Sweden)" ,"SD"),
    GLA_Glasgow("Glasgow Intl.", "Scotland (Paisley, Renfrewshire)" ,"UK"),
    NRT_Narita("Narita Intl.", "Narita (Narita, Chiba)" ,"JP"),
    ICN_Incheon("Incheon Intl.", "Jung District (Jung District, Incheon)" ,"SK");
    
    private final String city ;
    private final String country ;
    private final String airportName ;

    private AirPort(String airportName, String country,String city) {
        this.city = city;
        this.country = country;
        this.airportName = airportName;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getAirportName() {
        return airportName;
    }

}
