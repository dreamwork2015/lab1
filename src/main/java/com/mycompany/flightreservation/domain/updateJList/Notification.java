package com.mycompany.flightreservation.domain.updateJList;


public class Notification {

    
	private int SMS;
	private int smsEticket;
	private int notificationReminder;
	private int notificationDelay;
	private int notificationCancel;
	private int notificationDoorChange;
	private String phoneNumber;
        private double totalMomeySMS;

    public double getTotalMomeySMS() {
        return totalMomeySMS;
    }

    public void setTotalMomeySMS(double totalMomeySMS) {
        this.totalMomeySMS = totalMomeySMS;
    }
        
        
    public void setSMS(int SMS) {
        this.SMS = SMS;
    }

    public void setSmsEticket(int smsEticket) {
        this.smsEticket = smsEticket;
    }

    public void setNotificationReminder(int notificationReminder) {
        this.notificationReminder = notificationReminder;
    }

    public void setNotificationDelay(int notificationDelay) {
        this.notificationDelay = notificationDelay;
    }

    public void setNotificationCancel(int notificationCancel) {
        this.notificationCancel = notificationCancel;
    }

    public void setNotificationDoorChange(int notificationDoorChange) {
        this.notificationDoorChange = notificationDoorChange;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getSMS() {
        return SMS;
    }

    public int getSmsEticket() {
        return smsEticket;
    }

    public int getNotificationReminder() {
        return notificationReminder;
    }

    public int getNotificationDelay() {
        return notificationDelay;
    }

    public int getNotificationCancel() {
        return notificationCancel;
    }

    public int getNotificationDoorChange() {
        return notificationDoorChange;
    }

    public String getPhoneNumber() {
       return phoneNumber;
      
     
    }
	
	
}
