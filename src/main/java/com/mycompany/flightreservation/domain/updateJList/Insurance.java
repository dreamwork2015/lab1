/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.domain.updateJList;

/**
 *
 * @author Joy
 */
public class Insurance {
    private int InsuranceID;
    private double Amount;

    public Insurance(int InsuranceID, double Amount) {
        this.InsuranceID = InsuranceID;
        this.Amount = Amount;
    }

    
    
    public int getInsuranceID() {
        return InsuranceID;
    }

    public double getAmount() {
        return Amount;
    }

    public void setInsuranceID(int InsuranceID) {
        this.InsuranceID = InsuranceID;
    }

    public void setAmount(double Amount) {
        this.Amount = Amount;
    }
    
}
