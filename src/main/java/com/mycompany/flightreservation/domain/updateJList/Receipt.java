/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.domain.updateJList;

/**
 *
 * @author Joy
 */
public class Receipt{
	private String title;
	private String firstname;
	private String lastname;
	private String mobilephone;
	private String country;
	private String email;
        private String address;


	public String getTitle() {
              return  title;
            
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstname() {
              return firstname;
            
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
            
                return lastname;
            
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}


	public String getMobilephone() {
            return mobilephone;
            
	}

	public void setMobilephone(String mobilephone) {
		this.mobilephone = mobilephone;
	}


	public String getCountry() {
            return country;
           
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
           return email;
           
	}

	public void setEmail(String email) {
		this.email = email;
	}

        public String getAddress() {
              return address;
           
        }

        public void setAddress(String address) {
            this.address = address;
        }



}

