package com.mycompany.flightreservation.domain.updateJList;


public class Passenger{

	

	private String title;
	private String firstname;
	private String lastname;
	private String birthDay;
	private String passportNumber;
	private String country;
	private String passportExp;


	
	public String getTitle() {
                return title;
         
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstname() {
                return firstname;
           
	
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
           return lastname;	
        }
        
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	
	public String getPassportNumber() {
             return  passportNumber;
           
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public String getCountry() {
           return country;
            
	}

	public void setCountry(String country) {
		this.country = country;
	}


        public String getBirthDay() {
             return birthDay;
            
        }

        public String getExpPassport() {
            return passportExp;
        }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public void setPassportExp(String passportExp) {
        this.passportExp = passportExp;
    }

   
    
    

}
