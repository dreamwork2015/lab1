package com.mycompany.flightreservation.domain.updateJList;


public class Payment {

	private String typeOfCredit;
	private String numberOfCrediteCard;
	private String Expire;
        private double totalmoney;
        
	public String getTypeOfCredit() {
           return typeOfCredit;
            
	}
	public void setTypeOfCredit(String typeOfCredit) {
		this.typeOfCredit = typeOfCredit;
	}
	public String getNumberOfCrediteCard() {
            return numberOfCrediteCard;
           
	}
	public void setNumberOfCrediteCard(String numberOfCrediteCard) {
		this.numberOfCrediteCard = numberOfCrediteCard;
	}

    public String getExpire() {
        return Expire;
           
    }

    public double getTotalmoney() {
        return totalmoney;
    }

    public void setExpire(String Expire) {
        this.Expire = Expire;
    }

    public void setTotalmoney(double totalmoney) {
        this.totalmoney = totalmoney;
    }
	
	
}
